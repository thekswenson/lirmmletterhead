\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lirmmletter}[2020/11/04 LIRMM letterhead]

\RequirePackage{kvoptions}

\SetupKeyvalOptions{
  family=LIRMML,
  prefix=LIRMML@
}

\DeclareStringOption[33 (0)4 67 41 85 85]{lirmmtelephone}
\DeclareStringOption[]{lirmmpage}
\DeclareStringOption[Your Name]{lirmmname}
\DeclareStringOption[]{lirmmsig}

\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessKeyvalOptions*

\newcommand{\lirmmTelephone}{\LIRMML@lirmmtelephone~}

    % I cannot figure out how to allow the ~ character with kvoptions-patch,
    % so just assume the prefix "www.lirmm.fr/~" if something is given.
\ifx\LIRMML@lirmmpage\@empty
  \newcommand{\lirmmPage}{www.lirmm.fr}
\else
  \newcommand{\lirmmPage}{www.lirmm.fr/~\LIRMML@lirmmpage}
\fi

\LoadClass[]{letter}

    % Figure out lirmmclosing based on lirmmpage and lirmmsig
\ifx\LIRMML@lirmmsig\@empty
  \signature{\LIRMML@lirmmname}
  \newcommand{\lirmmclosing}[1]{\closing{#1}}
\else
  \newcommand{\lirmmclosing}[1]{
    \closing{#1\\
      \hspace{1mm}\fromsig{\includegraphics[height=14mm]{\LIRMML@lirmmsig}} \\
      \fromname{\LIRMML@lirmmname}
    }
  }
\fi

\RequirePackage[hidelinks]{hyperref}
\RequirePackage{graphicx}
\RequirePackage{fancyhdr}
\RequirePackage{geometry}
\RequirePackage{calc}
\RequirePackage{color}
\RequirePackage{marvosym}
\RequirePackage{url}
\RequirePackage{array}

\definecolor{fcolor}{gray}{1.00}

%_______________________________ The Footer _________________________________
\newcommand{\makefancyfooter}{
\fancyfoot[L]{\colorbox{fcolor}{\parbox[][\headheight][c]{\marginparwidth+2mm}{
  \hspace{17mm}\includegraphics[height=15mm]{fig/UMlogo.pdf}}
}}%

\fancyfoot[R]{\colorbox{fcolor}{\parbox[][\headheight][c]{\marginparwidth+2mm}{
  \hspace{0mm}\includegraphics[height=15mm]{fig/CNRSlogo.pdf}\hfil}
}}%

\fancyfoot[C]{\colorbox{fcolor}{\parbox[][\headheight][c]{\textwidth}{
  \fontsize{9pt}{12.0pt}\selectfont\centering %second number is 1.2 times first
  Laboratoire d’Informatique, de Robotique et de Microélectronique de Montpellier - UMR 5506 \\
  \bigskip
  {\setlength{\extrarowheight}{2mm}%
  \begin{tabular}{c}
  \hline
  \Letter~161 rue Ada, 34095 Montpellier \Telefon~\lirmmTelephone \Mundus~\url{\lirmmPage} \\[1mm]
  \hline
  \end{tabular}
  }
  }}}%
}

% _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ firstpage  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
\fancypagestyle{firstpage}{%
    \renewcommand{\headrulewidth}{0pt}%
    \renewcommand{\footrulewidth}{0pt}%
    \fancyhf{}%

    \fancyhead[L]{\parbox[t][\headheight][b]{\paperwidth}{
        \hspace{20mm}\includegraphics[height=18mm]{fig/LogoLIRMMlong.pdf}}
    }%

    \makefancyfooter
}%

% _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ empty  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
\fancypagestyle{empty}{%
    \renewcommand{\headrulewidth}{0pt}%
    \renewcommand{\footrulewidth}{0pt}%
    \fancyhf{}%

    \makefancyfooter
}%

% _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ plain  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
\fancypagestyle{plain}{%
    \renewcommand{\headrulewidth}{0pt}%
    \renewcommand{\footrulewidth}{0pt}%
    \fancyhf{}%

    \makefancyfooter
}



\setlength{\fboxsep}{0pt}
\geometry{top = 4.6cm, headheight = 4cm}
\geometry{bottom = 4.4cm}
\fancyhfoffset[L]{\oddsidemargin + \hoffset + 1in}
\fancyhfoffset[R]{\evensidemargin + \marginparwidth - \marginparsep}
