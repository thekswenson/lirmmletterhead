# A LaTeX letterhead for the LIRMM #

This is a LaTeX class file (_lirmmletter.cls_) that mimics the Microsoft Word letterhead given by the department. It makes writing a letter simple.

![Letterhead Screenshot](fig/letterhead_screenshot.png  "Letterhead Screenshot")

## How to use the lirmmletter.cls ##

1. Download the [sample project](https://bitbucket.org/thekswenson/lirmmletterhead/get/master.zip) zip file extract it.
1. Modify _sampleletter.tex_ by adding your text in place of the _\lipsum_ line.
1. Put your name after the _lirmmname=_.

If you want to personalize your phone number and web page, modify the _lirmmtelephone=_ and _lirmmpage=_ fields. Add a PDF of your signature by putting the path to the PDF file after _lirmmsig=_.
